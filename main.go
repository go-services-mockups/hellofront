package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
	"os"
        "strconv"
)

var espera_max int = 10
var ok_counter int = 1
var err_counter int = 1
var reset_time_global int = 60

func handler(w http.ResponseWriter, r *http.Request) {
	client := http.Client{
		Timeout : time.Second * time.Duration(espera_max),
	}
	resp, err := client.Get("http://backend:8888")
	if err != nil {
		fmt.Fprintln(w, "Opss, error reading backend\nError was: "+ err.Error())
		fmt.Println(err)
                err_counter = err_counter + 1
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Fprintln(w, "Hello ARQCONF \n"+string(body)+"\nThe response code was: " + string(resp.Status))
        ok_counter = ok_counter + 1
}

func HealthzHandler(w http.ResponseWriter, r *http.Request) {
	// Not a really good healthz check :S
	fmt.Fprintln(w, "OK - All good so far")
}

func MetricsHandler(w http.ResponseWriter, r *http.Request) {
        // Not a really good healthz check :S
        fmt.Fprintln(w, "ok_counter " + strconv.Itoa(ok_counter) + "\nerr_counter " + strconv.Itoa(err_counter) )
}

func main() {

        // Leemos del entorno las variables
        espera_max, _ = strconv.Atoi(os.Getenv("TIMEOUT"))

	http.HandleFunc("/", handler)
	http.HandleFunc("/healthz", HealthzHandler)
        http.HandleFunc("/metrics", MetricsHandler)
        go func(reset_time int) {
                       for {
                 fmt.Println("Reseting Metrics in " + strconv.Itoa(reset_time) + " Seconds")
                 time.Sleep(time.Duration(reset_time) * time.Second)
                 ok_counter = 0
                 err_counter = 0
                  }
        }(reset_time_global)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
